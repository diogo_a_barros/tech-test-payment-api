﻿using Payment.Domain.Entities;
using Payment.Domain.Queries;

namespace Payment.Domain.Tests.QueryTests
{
    [TestClass]
    public class VendaQueriesTests
    {
        [TestMethod]
        public void DadoUmIdDeveRetornarUmRegistroValido()
        {
            var vendedor = new Vendedor("000.000.000-00", "José", "josé@teste.com", "(00) 0 0000-000");
            var itens = new List<ItemVenda>
            {
                new ItemVenda("1", "Produto 1", 1.00, 1.00),
            };

            var lista = new List<Venda>()
            {
                new Venda(vendedor, itens),
                new Venda(vendedor, itens),
                new Venda(vendedor, itens),
                new Venda(vendedor, itens),
                new Venda(vendedor, itens),
                new Venda(vendedor, itens),
                new Venda(vendedor, itens),
                new Venda(vendedor, itens),
            };

            var id = lista[3].Id;

            var venda = lista.AsQueryable().Where(VendaQueries.GetById(id));

            Assert.IsNotNull(venda);
        }
    }
}
