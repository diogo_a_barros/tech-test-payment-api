﻿using Payment.Domain.Entities;

namespace Payment.Domain.Tests.EntityTests
{
    [TestClass]
    public class VendaTests
    {
        [TestMethod]
        public void AoCriarUmaNovaVendaOStatusDeveSerAguardandoPagamaneto()
        {
            var vendedor = new Vendedor("000.000.000-00", "José", "josé@teste.com", "(00) 0 0000-000");
            var itens = new List<ItemVenda>
            {
                new ItemVenda("1", "Produto 1", 1.00, 1.00)
            };

            var venda = new Venda(vendedor, itens);

            Assert.AreEqual(venda.Status, StatusVenda.AguardandoPagamento);
        }
    }
}
