﻿using Payment.Domain.Command;
using Payment.Domain.Entities;

namespace Payment.Domain.Tests.CommandTests
{
    [TestClass]
    public class CriarVendaCommandTests
    {
        [TestMethod]
        public void DadoUmObjetoComTodosOsCamposOMesmoDeveSerValido()
        {
            var vendedor = new Vendedor("000.000.000-00", "José", "josé@teste.com", "(00) 0 0000-000");
            var itens = new List<ItemVenda>
            {
                new ItemVenda("1", "Produto 1", 1.00, 1.00)
            };
            var comando = new CriarVendaCommand(vendedor, itens);
            Assert.IsTrue(comando.ValidationResult.IsValid);
        }

        [TestMethod]
        public void DadoUmObjetoSemVendedoOMesmoDeveSerInvalido()
        {
            var itens = new List<ItemVenda>
            {
                new ItemVenda("1", "Produto 1", 1.00, 1.00)
            };

            var comando = new CriarVendaCommand(null, itens);

            Assert.IsFalse(comando.ValidationResult.IsValid);
        }

        [TestMethod]
        public void DadoUmObjetoSemItensOMesmoDeveSerInvalido()
        {
            var vendedor = new Vendedor("000.000.000-00", "José", "josé@teste.com", "(00) 0 0000-000");
            var comando = new CriarVendaCommand(vendedor, null);
            Assert.IsFalse(comando.ValidationResult.IsValid);
        }
    }
}
