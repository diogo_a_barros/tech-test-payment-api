﻿using Payment.Domain.Command;
using Payment.Domain.Entities;

namespace Payment.Domain.Tests.CommandTests
{
    [TestClass]
    public class AlterarStatusCommandTests
    {
        [TestMethod]
        public void DadoUmObjetoComTodosOsCamposOMesmoDeveSerValido()
        {
            var Id = Guid.NewGuid();
            var status = StatusVenda.PagamentoAprovado;
            var comando = new AlterarStatusCommand(Id, status);

            Assert.IsTrue(comando.ValidationResult.IsValid);
        }

        [TestMethod]
        public void DadoUmObjetoSemIdOMesmoDeveSerInvalido()
        {
            var Id = Guid.Empty;
            var status = StatusVenda.PagamentoAprovado;
            var comando = new AlterarStatusCommand(Id, status);

            Assert.IsFalse(comando.ValidationResult.IsValid);
        }

        [TestMethod]
        public void DadoUmObjetoComStatusAguardandoPagamentoOMesmoDeveSerInvalido()
        {
            var Id = Guid.NewGuid();
            var status = StatusVenda.AguardandoPagamento;
            var comando = new AlterarStatusCommand(Id, status);

            Assert.IsFalse(comando.ValidationResult.IsValid);
        }
    }
}
