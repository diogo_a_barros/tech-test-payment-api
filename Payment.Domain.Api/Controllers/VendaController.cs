﻿using Microsoft.AspNetCore.Mvc;
using Payment.Domain.Command;
using Payment.Domain.Entities;
using Payment.Domain.Handlers;
using Payment.Domain.Repositories;

namespace Payment.Domain.Api.Controllers
{
    [ApiController]
    [Route("vendas")]
    public class VendaController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Venda> BuscarPorTodos([FromServices] IVendaRepository repository)
        {
            var vendas = repository.BuscarTodos();
            return vendas;
        }

        [HttpGet("{id}")]
        public Venda BuscarPorId(Guid id, [FromServices] IVendaRepository repository)
        {
            var venda = repository.BuscarPorId(id);
            return venda;
        }

        [HttpPost]
        public GenericCommandResult Criar(CriarVendaCommand command, [FromServices] VendaHandler handler)
        {
            return (GenericCommandResult)handler.Handle(command);
        }

        [HttpPut]
        public GenericCommandResult AlterarStatus(AlterarStatusCommand command, [FromServices] VendaHandler handler)
        {
            return (GenericCommandResult)handler.Handle(command);
        }
    }
}
