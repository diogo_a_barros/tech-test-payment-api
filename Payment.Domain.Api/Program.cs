using Microsoft.EntityFrameworkCore;
using Payment.Domain.Handlers;
using Payment.Domain.Infra.Context;
using Payment.Domain.Infra.Repositories;
using Payment.Domain.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

builder.Services.AddDbContext<ApplicationDbContext>(opcoes => opcoes.UseInMemoryDatabase("database"));

builder.Services.AddTransient<IVendaRepository, VendaRepository>();
builder.Services.AddTransient<VendaHandler, VendaHandler>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

app.UseAuthorization();

app.MapControllers();

app.Run();
