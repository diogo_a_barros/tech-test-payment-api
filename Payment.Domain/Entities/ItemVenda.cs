namespace Payment.Domain.Entities
{
    public class ItemVenda : EntidadeBase
    {
        public ItemVenda(string codigo, string descricao, double quantidade, double vlrUnitario)
        {
            Codigo = codigo;
            Descricao = descricao;
            Quantidade = quantidade;
            VlrUnitario = vlrUnitario;
        }

        public string Codigo { get; private set; }
        public string Descricao { get; private set; }
        public double Quantidade { get; private set; }
        public double VlrUnitario { get; private set; }

        public double VlrTotal()
        {
            return Quantidade * VlrUnitario;
        }

        public void AlterarQuantidade(double quantidade)
        {
            if (quantidade <= 0)
            {
                throw new Exception("A quantidade deve ser maior que zero.");
            }
            Quantidade = quantidade;
        }
    }
}