namespace Payment.Domain.Entities
{
    public enum StatusVenda
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoTransportadora,
        Entregue,
        Cancelada
    }
}