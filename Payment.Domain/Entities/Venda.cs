namespace Payment.Domain.Entities
{
    public class Venda : EntidadeBase
    {
        public Venda()
        {

        }

        public Venda(Vendedor vendedor, IList<ItemVenda> itens)
        {
            Vendedor = vendedor;
            Itens = itens;
            Data = DateTime.Now;
            Status = StatusVenda.AguardandoPagamento;
        }

        public Vendedor Vendedor { get; private set; }
        public DateTime Data { get; private set; }
        public IList<ItemVenda> Itens { get; private set; }
        public StatusVenda Status { get; private set; }

        public void AlterarStatus(StatusVenda novoStatus)
        {
            Status = novoStatus;
        }
    }
}