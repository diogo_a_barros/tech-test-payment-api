namespace Payment.Domain.Entities
{
    public class Vendedor : EntidadeBase
    {
        public Vendedor( string cpf, string nome, string email, string telefone)
        {
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }
        
        public string Cpf { get; private set; }
        public string Nome { get; private set; } 
        public string Email { get; private set; }
        public string Telefone { get; private set; }

        public void AlterarEmail(string email) 
        {
            Email = email;
        }

        public void AlterarTelefone(string telefone) 
        {
            Telefone = telefone;
        }
    }
}