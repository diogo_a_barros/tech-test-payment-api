﻿using Payment.Domain.Entities;

namespace Payment.Domain.Repositories
{
    public interface IVendaRepository
    {
        void Inserir(Venda venda);
        void Alterar(Venda venda);
        Venda BuscarPorId(Guid idVenda);
        IEnumerable<Venda> BuscarTodos();
    }
}
