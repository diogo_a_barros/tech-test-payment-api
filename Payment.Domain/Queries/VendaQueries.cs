﻿using Payment.Domain.Entities;
using System.Linq.Expressions;

namespace Payment.Domain.Queries
{
    public static class VendaQueries
    {
        public static Expression<Func<Venda, bool>> GetById(Guid Id)
        {
            return x => x.Id == Id;
        }
    }
}
