﻿using Payment.Domain.Command.Contracts;

namespace Payment.Domain.Handlers.Contracts
{
    public interface IHandler<T> where T : ICommand
    {
        ICommandResult Handle(T command);
    }
}
