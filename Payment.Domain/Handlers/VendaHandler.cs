﻿using Payment.Domain.Command;
using Payment.Domain.Command.Contracts;
using Payment.Domain.Entities;
using Payment.Domain.Handlers.Contracts;
using Payment.Domain.Repositories;

namespace Payment.Domain.Handlers
{
    public class VendaHandler : IHandler<CriarVendaCommand>, IHandler<AlterarStatusCommand>
    {
        private readonly IVendaRepository _repository;

        public VendaHandler(IVendaRepository repository)
        {
            _repository = repository;
        }

        public ICommandResult Handle(CriarVendaCommand command)
        {
            if (!command.ValidationResult.IsValid)
            {
                var mensagem = command.ValidationResult.Errors.ToString();
                if (mensagem == null)
                {
                    mensagem = "Erro ao salvar o registro";
                }
                return new GenericCommandResult(false, mensagem, command);
            }

            var venda = new Venda(command.Vendedor, command.Items);
            
            _repository.Inserir(venda);

            return new GenericCommandResult(true, "Registro salvo com sucesso", command);
        }

        public ICommandResult Handle(AlterarStatusCommand command)
        {
            if (!command.ValidationResult.IsValid)
            {
                var mensagem = command.ValidationResult.Errors.ToString();
                if (mensagem == null)
                {
                    mensagem = "Erro ao salvar o registro";
                }
                return new GenericCommandResult(false, mensagem, command);
            }

            var vendaBanco = _repository.BuscarPorId(command.Id);

            if (vendaBanco == null)
            {
                return new GenericCommandResult(false, "A venda não foi localizada no banco de dados", command);
            }

            switch (vendaBanco.Status)
            {
                case StatusVenda.AguardandoPagamento:
                    if (command.StatusVenda == StatusVenda.AguardandoPagamento || command.StatusVenda == StatusVenda.EnviadoTransportadora || command.StatusVenda == StatusVenda.Entregue)
                    {
                        return new GenericCommandResult(false, $"O status {vendaBanco.Status} não pode ser alterado para {command.StatusVenda}", command);
                    }
                    break;
                case StatusVenda.PagamentoAprovado:
                    if (command.StatusVenda == StatusVenda.AguardandoPagamento || command.StatusVenda == StatusVenda.PagamentoAprovado || command.StatusVenda == StatusVenda.Entregue)
                    {
                        return new GenericCommandResult(false, $"O status {vendaBanco.Status} não pode ser alterado para {command.StatusVenda}", command);
                    }
                    break;
                case StatusVenda.EnviadoTransportadora:
                    if (command.StatusVenda != StatusVenda.Entregue)
                    {
                        return new GenericCommandResult(false, $"O status {vendaBanco.Status} não pode ser alterado para {command.StatusVenda}", command);
                    }
                    break;
                case StatusVenda.Entregue:
                    return new GenericCommandResult(false, $"O status {vendaBanco.Status} não pode ser alterado para {command.StatusVenda}", command);
                case StatusVenda.Cancelada:
                    return new GenericCommandResult(false, $"O status {vendaBanco.Status} não pode ser alterado para {command.StatusVenda}", command);
            }

            vendaBanco.AlterarStatus(command.StatusVenda);

            _repository.Alterar(vendaBanco);

            return new GenericCommandResult(true, "Registro salvo com sucesso", command);
        }
    }
}
