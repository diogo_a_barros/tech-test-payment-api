﻿using FluentValidation;
using FluentValidation.Results;
using Payment.Domain.Command.Contracts;
using Payment.Domain.Entities;
using System.Text.Json.Serialization;

namespace Payment.Domain.Command
{
    public class CriarVendaCommand : ICommand
    {
        public CriarVendaCommand(Vendedor vendedor, IList<ItemVenda> items)
        {
            Vendedor = vendedor;
            Items = items;
        }

        public Vendedor Vendedor { get; set; }
        public IList<ItemVenda> Items { get; set; }

        private ValidationResult validationResult = new ValidationResult();

        [JsonIgnore]
        public ValidationResult ValidationResult
        {
            get
            {
                var erros = new CriarVendaCommandValidator().Validate(this).Errors;
                validationResult.Errors.AddRange(erros);
                return validationResult;
            }
        }

        private class CriarVendaCommandValidator : AbstractValidator<CriarVendaCommand>
        {
            public CriarVendaCommandValidator()
            {
                RuleFor(x => x.Vendedor).Must(ValidarVendedor).WithMessage("O Vendedor informado é inválido.");
                RuleFor(x => x.Items).Must(ValidarItens).WithMessage("Não foram informados itens para a venda");
            }

            private bool ValidarItens(IList<ItemVenda> items)
            {
                if (items == null)
                {
                    return false;
                }
                if (items.Count == 0)
                {
                    return false;
                }
                return true;
            }

            private bool ValidarVendedor(Vendedor vendedor)
            {
                if (vendedor == null)
                {
                    return false;
                }
                if (vendedor.Id == Guid.Empty)
                {
                    return false;
                }
                if (vendedor.Cpf.Equals(""))
                {
                    return false;
                }
                if (vendedor.Email.Equals(""))
                {
                    return false;
                }
                if (vendedor.Telefone.Equals(""))
                {
                    return false;
                }
                if (vendedor.Nome.Equals(""))
                {
                    return false;
                }
                return true;
            }
        }
    }
}
