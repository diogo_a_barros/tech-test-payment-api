﻿using FluentValidation.Results;

namespace Payment.Domain.Command.Contracts
{
    public interface ICommand
    {
        ValidationResult ValidationResult { get; }

    }
}
