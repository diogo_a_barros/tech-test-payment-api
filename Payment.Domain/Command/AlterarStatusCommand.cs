﻿using FluentValidation;
using FluentValidation.Results;
using Payment.Domain.Command.Contracts;
using Payment.Domain.Entities;
using System.Text.Json.Serialization;

namespace Payment.Domain.Command
{
    public class AlterarStatusCommand : ICommand
    {
        public AlterarStatusCommand(Guid id, StatusVenda statusVenda)
        {
            Id = id;
            StatusVenda = statusVenda;
        }

        public Guid Id { get; set; }
        public StatusVenda StatusVenda { get; set; }

        private ValidationResult validationResult = new ValidationResult();

        [JsonIgnore]
        public ValidationResult ValidationResult
        {
            get
            {
                var erros = new AlterarStatusCommandValidator().Validate(this).Errors;
                validationResult.Errors.AddRange(erros);
                return validationResult;
            }
        }

        private class AlterarStatusCommandValidator : AbstractValidator<AlterarStatusCommand>
        {
            public AlterarStatusCommandValidator()
            {
                RuleFor(x => x.Id).NotEqual(Guid.Empty).WithMessage("O id da venda não foi informado.");
                RuleFor(x => x.StatusVenda).NotEqual(StatusVenda.AguardandoPagamento).WithMessage("O status informado é invalido.");
            }
        }
    }
}
