﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Domain.Infra.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Venda> Vendas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>().ToTable("Venda");
            modelBuilder.Entity<Venda>().Property(x => x.Id);
            modelBuilder.Entity<Venda>().HasOne(x => x.Vendedor).WithMany();
            modelBuilder.Entity<Venda>().HasMany(x => x.Itens).WithOne();
            modelBuilder.Entity<Venda>().Navigation(x => x.Itens).UsePropertyAccessMode(PropertyAccessMode.Property);
        }
    }
}
