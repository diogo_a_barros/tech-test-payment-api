﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;
using Payment.Domain.Infra.Context;
using Payment.Domain.Queries;
using Payment.Domain.Repositories;

namespace Payment.Domain.Infra.Repositories
{
    public class VendaRepository : IVendaRepository
    {

        private readonly ApplicationDbContext _context;

        public VendaRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Alterar(Venda venda)
        {
            _context.Vendas.Update(venda);
            _context.SaveChangesAsync();
        }

        public Venda BuscarPorId(Guid idVenda)
        {
            var venda = _context.Vendas.Include(x => x.Itens).Include(x => x.Vendedor).FirstOrDefault(VendaQueries.GetById(idVenda));
            return venda;
        }

        public IEnumerable<Venda> BuscarTodos()
        {
            return _context.Vendas.Include(x => x.Itens).Include(x => x.Vendedor).ToList();
        }

        public void Inserir(Venda venda)
        {
            _context.Vendas.Add(venda);
            _context.SaveChanges();
        }
    }
}
